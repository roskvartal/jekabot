import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'world',
		id: '6892-3dbe-i92p-1002'
	}
});

window.app = app;

export default app;