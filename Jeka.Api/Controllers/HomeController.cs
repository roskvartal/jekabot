using Microsoft.AspNetCore.Mvc;

namespace Jeka.Api.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}