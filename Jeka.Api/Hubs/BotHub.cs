﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Jeka.Api.Hubs
{
    public class BotHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            var answers = new [] 
            {
                "Прювет!", "Сам такой", "Хай", "неа", "Дурак штоле?", "кк", "Меня зовут Жека"
            };

            await Clients.Client(Context.ConnectionId)
                .SendAsync("ReceiveMessage", "JekaBot", answers[new Random().Next(0,7)]);
        }
    }
}